package br.ufal.ic.main;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringHibernateMain {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"spring.xml");
		
		System.out.println("BANCO CARREGADO");
		
		context.close();
	}

}
