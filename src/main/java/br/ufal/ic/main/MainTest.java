package br.ufal.ic.main;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import br.ufal.ic.dao.ClientDAO;
import br.ufal.ic.dao.impl.StudentDAOImpl;
import br.ufal.ic.model.Client;

public class MainTest {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"spring.xml");
		ClientDAO clientDAO = context.getBean(ClientDAO.class);
//		StudentDAOImpl


		/*
		 * SAVE CLIENT - STUDENT
		 */
		Client client1 = new Client();
		client1.setAddress("Rua do Sol");
		client1.setEmail("lucas@email.com");
		client1.setName("Lucas");
		client1.setPhone_number("8888-8888");
		client1.setType_client("ALUNO");
		client1.setCpf("00000000");
		
		clientDAO.save(client1);
		
		
	
//		
//		Client client2 = new Client();
//		clientType = clientTypeDAO.findById(2);
//		client2.setClientType(clientType);
//		client2.setAndress("Rua das Árvores");
//		client2.setEmail("rodrigo@email.com");
//		client2.setName("Rodrigo");
//		client2.setPhone_number("9999-9999");
//		
//		
//		Client client3 = new Client();
//		clientType = clientTypeDAO.findById(3);
//		client3.setClientType(clientType);
//		client3.setAndress("Rua");
//		client3.setEmail("kin@email.com");
//		client3.setName("Kin");
//		client3.setPhone_number("9898-9898");
		

//		clientDAO.save(client2);
//		clientDAO.save(client3);
//		

		context.close();
	}

}
