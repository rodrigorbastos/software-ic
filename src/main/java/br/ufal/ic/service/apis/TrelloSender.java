package br.ufal.ic.service.apis;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.*;

/**
 * Created by rodrigo on 16/07/15.
 */
public class TrelloSender {

    protected JmsTemplate jmsTemplate;

    public JmsTemplate getJmsTemplate()
    {
        return jmsTemplate;
    }

    public void setJmsTemplate(JmsTemplate jmsTemplate)
    {
        this.jmsTemplate = jmsTemplate;
    }

    public void sendMessages() throws JMSException
    {
        System.out.println("PRODUCER");
        MessageCreator messageCreator = new MessageCreator()
        {
            public Message createMessage(Session session) throws JMSException
            {
                TextMessage message = session.createTextMessage("Mãozinha até o chão");
                return message;
            }
        };
        jmsTemplate.send(messageCreator);
    }

    public void createActivity(final String name, final String desc) throws JMSException{
        System.out.println("PRODUCER");
        MessageCreator messageCreator = new MessageCreator() {

            public Message createMessage(Session session) throws JMSException {
                MapMessage message = session.createMapMessage();
                message.setString("name", name);
                message.setString("desc", desc);
                return message;
            }

        };

        jmsTemplate.send(messageCreator);

    }
}
