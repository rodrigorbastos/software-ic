package br.ufal.ic.service.apis.mail;

/**
 * Created by lucas on 21/07/15.
 */

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.io.IOException;

public class MailListener implements MessageListener {

public void onMessage(Message message) {
        System.out.println("RECEIVER");

        MapMessage mapMessage = (MapMessage) message;

        try {
                String to = mapMessage.getString("to");
                String context = mapMessage.getString("context");
                APIMail mail = new APIMail(context, to);
                mail.sendMail();
        } catch (JMSException e) {
                e.printStackTrace();
        }




        }
}