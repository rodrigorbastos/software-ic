
package br.ufal.ic.service.apis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by lucas on 13/07/15.
 */
public class APITrello {

    private String name;
    private String descrition;
    private String urlFull;
    private URL obj;
    private HttpURLConnection con;

    public APITrello(String name, String desc)
    {
        this.name = name;
        this.descrition = desc;
    }

    public void setURL() throws IOException
    {
        name = URLEncoder.encode(name, "UTF-8");
        descrition = URLEncoder.encode(descrition, "UTF-8");
        urlFull = "https://api.trello.com/1/lists/55a448909010d0c530e0ac7d/cards?key=08b8416eb79e3e8e082b884afa54de27&token=8f223d0de85368a04a747495ca9d7fdb75092499593251f3801bd3c422ead8c1&name="+this.name+"&desc="+this.descrition+"&idList=55a448909010d0c530e0ac7d";
    }

    public void sendMessage() throws IOException
    {
        obj = new URL(urlFull);
        con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("POST");

    }

    public void sendConfirm() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer(1000);

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());
    }
}
