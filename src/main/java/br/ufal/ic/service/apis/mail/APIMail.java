package br.ufal.ic.service.apis.mail;

import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.PasswordAuthentication;

/**
 * Created by lucas on 21/07/15.
 */
public class APIMail {

    String username = "icufalfake@gmail.com";
    String password = "123456ic";
    Session session;

    String context;
    String to;

    public APIMail(String context, String to)
    {
        this.context = context;
        this.to = to;

        setConfigMail();

    }

    public void setConfigMail (){
        Properties props = new Properties();
        props.put("mail.smtp.host" , "smtp.gmail.com");
        props.put("mail.stmp.user" , "context");

        //To use TLS
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.to", "to");
        //To use SSL
        props.put("mail.smtp.socketFactory.port", "587");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "587");

        session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(APIMail.this.username, password);
            }
        });

    }

    public void sendMail()
    {
        javax.mail.Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(username));
            msg.setRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));
            msg.setSubject("[INFO] - Secretaria IC");
            msg.setText(context);

            Transport transport = session.getTransport("smtp");
            System.out.println("aqui");
            transport.connect("smtp.gmail.com" , 587 , "context", "to");
            transport.send(msg);
            System.out.println("fine!!");
        }
        catch(Exception exc) {
            System.out.println(exc);
        }
    }

}
