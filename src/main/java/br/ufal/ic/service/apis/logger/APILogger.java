package br.ufal.ic.service.apis.logger;

/**
 * Created by lucas on 20/07/15.
 */

import java.io.File;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxEntry;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWriteMode;

public class APILogger {

    private String date;
    private String serviceType;
    private String crud;
    private String user;
    private int day,year,month;
    private APIDropBox apiDropBox;
    String logContent, newLogName;
    String pathFile;

    public APILogger(String date, String serviceType, String crud, String user)
    {
        this.date = date;
        this.serviceType = serviceType;
        this.crud = crud;
        this.user = user;
        logContent = date+";"+serviceType+";"+crud+";"+user;

        apiDropBox = new APIDropBox();
        ClassLoader classLoader = getClass().getClassLoader();
        pathFile = classLoader.getResource("log.txt").getPath();
        try {
            this.setConfigLogger();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    //TODO acrescentar hora
    public void setConfigLogger() throws ParseException {
        Calendar cal = Calendar.getInstance();
        Date dateFormat = new SimpleDateFormat("yyyy/MM/dd").parse(date);
        cal.setTime(dateFormat);

        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH) + 1;
        year = cal.get(Calendar.YEAR);
        newLogName = year+"-"+month+"-"+day+"_SI_LOG.csv";
        System.out.println(day);
    }


    public void readFile(){

        LogFile logFile = new LogFile();
        logFile.readFile(pathFile);

        //Today Logger
        if ((year == logFile.getYear()) && (month == logFile.getMonth()) && (day == logFile.getDay())) {
            logFile.writeFile(logContent, pathFile, true);
        }else{
            //Tommorrow Logger

            File inputFile = new File(pathFile);
            try {
                apiDropBox.uploadFile(inputFile, newLogName);

                //manda conteúdo e log.txt
                logFile.writeFile(logContent, pathFile, false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public String toString() {
        return "APILogger{" +
                "date=" + date +
                ", serviceType='" + serviceType + '\'' +
                ", crud='" + crud + '\'' +
                ", user='" + user + '\'' +
                '}';
    }
}
