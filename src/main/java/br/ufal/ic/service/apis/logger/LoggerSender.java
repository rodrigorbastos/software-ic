package br.ufal.ic.service.apis.logger;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.*;
import java.util.Date;

/**
 * Created by lucas on 20/07/15.
 */
public class LoggerSender {

    protected JmsTemplate jmsTemplate;

    public JmsTemplate getJmsTemplate()
    {
        return jmsTemplate;
    }

    public void setJmsTemplate(JmsTemplate jmsTemplate)
    {
        this.jmsTemplate = jmsTemplate;
    }


    public void writeLog(final String date, final String serviceType, final String crud, final String user) throws JMSException{
        System.out.println("PRODUCER");
        MessageCreator messageCreator = new MessageCreator() {

            public Message createMessage(Session session) throws JMSException {
                MapMessage message = session.createMapMessage();
                message.setString("date", date);
                message.setString("serviceType", serviceType);
                message.setString("crud", crud);
                message.setString("user", user);
                return message;
            }

        };

        jmsTemplate.send(messageCreator);

    }
}
