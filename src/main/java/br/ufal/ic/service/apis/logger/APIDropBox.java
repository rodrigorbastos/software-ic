package br.ufal.ic.service.apis.logger;

import com.dropbox.core.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by lucas on 20/07/15.
 */
public class APIDropBox {

    private DbxRequestConfig config = new DbxRequestConfig("Secretaria IC", Locale.getDefault().toString());
    private String token = "Gpa4XqWy4PwAAAAAAAABHit1jXIkCW0hpsa8cjY6B3AhCXG_EX3N6pLFQaevfV2b";//token da api do dropbox
    DbxClient client;
    public APIDropBox (){
        client = new DbxClient(config, token);
    }


    public void uploadFile(File inputFile, String nameFile) throws IOException {

        String dirfile = "/"+nameFile;
        FileInputStream inputStream = new FileInputStream(inputFile);
        try {
            DbxEntry.File uploadedFile = client.uploadFile(dirfile, DbxWriteMode.add(), inputFile.length(), inputStream);
            System.out.println("Uploaded: " + uploadedFile.toString());
        } catch (DbxException e) {
            e.printStackTrace();
        } finally {
            inputStream.close();
        }
    }
}
