package br.ufal.ic.service.apis.mail;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;

/**
 * Created by lucas on 21/07/15.
 */
public class MailSender {

    protected JmsTemplate jmsTemplate;

    public JmsTemplate getJmsTemplate()
    {
        return jmsTemplate;
    }

    public void setJmsTemplate(JmsTemplate jmsTemplate)
    {
        this.jmsTemplate = jmsTemplate;
    }


    public void sendMail(final String context, final String to) throws JMSException {
        System.out.println("PRODUCER");
        MessageCreator messageCreator = new MessageCreator() {

            public Message createMessage(Session session) throws JMSException {
                MapMessage message = session.createMapMessage();
                message.setString("context", context);
                message.setString("to", to);
                return message;
            }

        };

        jmsTemplate.send(messageCreator);

    }
}
