package br.ufal.ic.service.apis.logger;

import br.ufal.ic.service.apis.APITrello;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.io.IOException;

/**
 * Created by lucas on 20/07/15.
 */
public class LoggerListener implements MessageListener {

    public void onMessage(Message message) {
        System.out.println("RECEIVER");

        MapMessage mapMessage = (MapMessage) message;

        try {
            // obtem dados da mensagem
            String date = mapMessage.getString("date");
            String serviceType = mapMessage.getString("serviceType");
            String crud = mapMessage.getString("crud");
            String user = mapMessage.getString("user");
            APILogger logger = new APILogger(date, serviceType, crud, user);
            System.out.println(logger.toString());
            logger.readFile();


        }catch (JMSException e) {
            e.printStackTrace();
        }

    }
}
