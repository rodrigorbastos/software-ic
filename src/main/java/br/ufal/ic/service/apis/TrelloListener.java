package br.ufal.ic.service.apis;

import javax.jms.*;
import java.io.IOException;

/**
 * Created by rodrigo on 16/07/15.
 */
public class TrelloListener implements MessageListener {

    public void onMessage(Message message) {
        System.out.println("RECEIVER");

        MapMessage mapMessage = (MapMessage) message;

        try {
            // obtem dados da mensagem
            String name = mapMessage.getString("name");
            String desc = mapMessage.getString("desc");
            APITrello trello = new APITrello(name,desc);
            trello.setURL();
            trello.sendMessage();
            trello.sendConfirm();

        }catch (JMSException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
