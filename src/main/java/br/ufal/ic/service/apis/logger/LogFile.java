package br.ufal.ic.service.apis.logger;

/**
 * Created by lucas on 20/07/15.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class LogFile {

    private String dateDayFile = "", dateMonthFile = "", dateYearFile = "";
    private String dateDay = "", dateMonth = "", dateYear = "";
    private int day, month, year;
    private boolean dateEquals = false;

    public LogFile(){

    }

    //pega a data do log.txt
    public void readFile(String pathFileLog)
    {
        String linha = ""; // lê a primeira linha
        String dateFile = ""; //pega a data do arquivo
        char caracterLinha;
        int index = 0;

        try{

            FileReader arq = new FileReader(pathFileLog);
            BufferedReader lerArq = new BufferedReader(arq);
            linha = lerArq.readLine();
            //System.out.println("1ª Linha: " + linha);
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        caracterLinha = linha.charAt(index);

        //pega apenas a data da linha inicial
        while(caracterLinha != ';'){
            dateFile += caracterLinha;
            index++;
            caracterLinha = linha.charAt(index);
        }

        this.year = Integer.parseInt(dateFile.substring(0, 4));
        this.month = Integer.parseInt(dateFile.substring(5, 7));
        this.day = Integer.parseInt(dateFile.substring(8, 10));

    }


    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    //acrescenta centeudo no arquivo existente
    public void writeFile(String textFile, String path, boolean flag)
    {
        System.out.println("write File" + " " + flag + " " + path);
        try {
            // O parametro é que indica se deve sobrescrever ou continua no
            // arquivo.
            FileWriter fw = new FileWriter(path);
            if(flag){
               fw = new FileWriter(path, flag);
            }

            BufferedWriter conexao = new BufferedWriter(fw);
            conexao.write(textFile);
            conexao.newLine();
            conexao.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void print() {
        System.out.println("Files [dateDayFile=" + dateDayFile + ", dateMonthFile="
                + dateMonthFile + ", dateYearFile=" + dateYearFile
                + ", dateDay=" + dateDay + ", dateMonth=" + dateMonth
                + ", dateYear=" + dateYear + "] igualdade:" + dateEquals);
    }
}
