package br.ufal.ic.service.facades;

import br.ufal.ic.model.Classroom;
import br.ufal.ic.model.ClassroomReserve;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by lucas on 21/07/15.
 */
public interface ClassroomReserveFacade {

    /*
	 * CRUD Classroom Reserve
	 */
    ClassroomReserve save(ClassroomReserve classroomReserve) throws Exception;

    ClassroomReserve update(ClassroomReserve classroomReserve) throws Exception;

    ResponseEntity<Serializable> delete(Integer id) throws Exception;




    /*
     * AUXILIARES
     */
    List<ClassroomReserve> findAll();

    ClassroomReserve findById(int id) throws Exception;
}
