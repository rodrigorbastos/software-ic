package br.ufal.ic.service.facades;

import br.ufal.ic.model.Employees;
import br.ufal.ic.model.Student;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by lucas on 21/07/15.
 */
public interface EmployeesFacade {

    /*
	 *CRUD STUDENT
	 */
    public Employees save(Employees employees) throws Exception;

    public Employees update(Employees employees) throws Exception;

    public ResponseEntity<Serializable> delete(Integer employees_id) throws Exception;

	/*
	 * AUXILIARES
	 */

    public List<Employees> findAll();

    public Employees findById(int id) throws Exception;
}
