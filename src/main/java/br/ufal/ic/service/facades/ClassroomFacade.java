package br.ufal.ic.service.facades;

import br.ufal.ic.model.Classroom;
import br.ufal.ic.model.MaterialType;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by lucas on 21/07/15.
 */
public interface ClassroomFacade {

    /*
	 * CRUD Classroom
	 */
    Classroom save(Classroom classroom) throws Exception;

    Classroom update(Classroom classroom) throws Exception;

    ResponseEntity<Serializable> delete(Integer id) throws Exception;




    /*
     * AUXILIARES
     */
    List<Classroom> findAll();

    Classroom findById(int id) throws Exception;
}
