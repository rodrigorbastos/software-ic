package br.ufal.ic.service.facades.impl;

import br.ufal.ic.dao.MaterialReserveDAO;
import br.ufal.ic.model.Client;
import br.ufal.ic.model.Material;
import br.ufal.ic.model.MaterialReserve;
import br.ufal.ic.model.MaterialType;
import br.ufal.ic.service.apis.logger.LoggerSender;
import br.ufal.ic.service.facades.MaterialReserveFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by rodrigo on 19/07/15.
 */
public class MaterialReserveController implements MaterialReserveFacade{

    @Autowired
    private MaterialReserveDAO materialReserveDAO;

    @Autowired
    LoggerSender logSender;

    private String serviceType = "MaterialReserve";

    public MaterialReserve save(MaterialReserve materialReserve) throws Exception {
        Client client = materialReserve.getClient();
        Material material = materialReserve.getMaterial();
        //Criar toda lógica de cadastro de material

        //LOGGER
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = date_format.format(new Date());

        logSender.writeLog(date, serviceType+": "+materialReserve.getId(), "save", "admin");

        return materialReserveDAO.save(materialReserve);
    }

    public MaterialReserve update(MaterialReserve materialReserve) throws Exception {

        //LOGGER
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = date_format.format(new Date());

        logSender.writeLog(date, serviceType+": "+materialReserve.getId(), "update", "admin");

        return materialReserveDAO.update(materialReserve);
    }

    public ResponseEntity<Serializable> delete(Integer materialReserve_id) throws Exception {
        MaterialReserve materialReserve = materialReserveDAO.findById(materialReserve_id);

        if(materialReserve != null){
            materialReserveDAO.delete(materialReserve);

            //LOGGER
            SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            String date = date_format.format(new Date());

            logSender.writeLog(date, serviceType+": "+materialReserve.getId(), "delete", "admin");

            return new ResponseEntity<Serializable>(
                    "MaterialReserve deletado com sucesso", HttpStatus.OK);
        }


        return new ResponseEntity<Serializable>(
                "Reserva do Material não encontrada", HttpStatus.BAD_REQUEST);

    }

    public List<MaterialReserve> findAll() {
        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        try {
            logSender.writeLog(date, serviceType+": list", "read", "admin");
        } catch (JMSException e) {
            e.printStackTrace();
        }

        return materialReserveDAO.findAll();
    }

    public ResponseEntity<Serializable> findById(int id) throws Exception {
        MaterialReserve materialReserve = materialReserveDAO.findById(id);
        if(materialReserve != null){

            //LOGGER
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            String date = dateFormat.format(new Date());

            logSender.writeLog(date, serviceType+": list", "read", "admin");

            return new ResponseEntity<Serializable>(materialReserve,
                    HttpStatus.OK);
        }
        return new ResponseEntity<Serializable>(
                "Reserva do Material não encontrada", HttpStatus.BAD_REQUEST);
    }

}
