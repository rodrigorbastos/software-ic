package br.ufal.ic.service.facades;

import br.ufal.ic.model.Teacher;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by rodrigo on 19/07/15.
 */
public interface TeacherFacade {

    /*
	 *CRUD STUDENT
	 */
    public Teacher save(Teacher teacher) throws Exception;

    public Teacher update(Teacher teacher) throws Exception;

    public ResponseEntity<Serializable> delete(Integer teacher_id) throws Exception;

	/*
	 * AUXILIARES
	 */

    public List<Teacher> findAll();

    public ResponseEntity<Serializable> findById(int teacher_id) throws Exception;

}
