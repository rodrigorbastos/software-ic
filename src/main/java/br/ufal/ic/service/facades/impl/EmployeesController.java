package br.ufal.ic.service.facades.impl;

import br.ufal.ic.dao.ClientDAO;
import br.ufal.ic.dao.EmployeesDAO;
import br.ufal.ic.dao.MaterialReserveDAO;
import br.ufal.ic.model.*;
import br.ufal.ic.service.apis.logger.LoggerSender;
import br.ufal.ic.service.exception.CustomGenericException;
import br.ufal.ic.service.facades.DocFacade;
import br.ufal.ic.service.facades.EmployeesFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by lucas on 21/07/15.
 */
public class EmployeesController implements EmployeesFacade {

    @Autowired
    private ClientDAO clientDAO;

    @Autowired
    private EmployeesDAO employeesDAO;

    @Autowired
    MaterialReserveDAO materialReserveDAO;

    @Autowired
    LoggerSender logSender;

    @Autowired
    DocFacade docFacade;

    private String serviceType = "Funcionario";

    public Employees save(Employees employees) throws Exception {
        Client client = clientDAO.save(employees.getClient());
        employees.setClient(client);
		// SAVE Employees
		employees = employeesDAO.save(employees);

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        logSender.writeLog(date, serviceType+": "+employees.getId(), "save", "admin");

        return employees;
    }

    public Employees update(Employees employees) throws Exception {

        Client client = employees.getClient();
        client = clientDAO.update(client);

        // UPDATE EMPLOYEE
        employees.setClient(client);
        employees = employeesDAO.update(employees);

        //LOGGER
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = date_format.format(new Date());

        logSender.writeLog(date, serviceType+": "+employees.getId(), "update", "admin");

        return employees;
    }

    public ResponseEntity<Serializable> delete(Integer employees_id) throws Exception {
        Employees employees = employeesDAO.findById(employees_id);

        // GET CLIENT
        Client client = employees.getClient();


        MaterialReserve mr = materialReserveDAO.findByIdClient(client);
        Document doc = docFacade.findByClient(client);

        if (mr == null && doc == null) {

            employeesDAO.delete(employees);
            clientDAO.delete(client);

            //LOGGER
            SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            String date = date_format.format(new Date());

            logSender.writeLog(date, serviceType+": "+employees.getId(), "delete", "admin");

            return new ResponseEntity<Serializable>(
                    "{ \n\t\"sucesso\": \"Funcionário deletado com sucesso\" \n}",
                    HttpStatus.OK);
        }

        return new ResponseEntity<Serializable>(new CustomGenericException(
                "" + HttpStatus.NOT_ACCEPTABLE.value(),
                "Funcionário não deletado: dependências ativas"),
                HttpStatus.NOT_ACCEPTABLE);
    }

    public List<Employees> findAll() {

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        try {
            logSender.writeLog(date, serviceType+": list", "read", "admin");
        } catch (JMSException e) {
            e.printStackTrace();
        }

        return employeesDAO.findAll();

    }

    public Employees findById(int id) throws Exception {
        Employees employees = null;
        employees = employeesDAO.findById(id);

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        logSender.writeLog(date, serviceType+": list", "read", "admin");

        return employees;
    }
}
