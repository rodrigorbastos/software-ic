package br.ufal.ic.service.facades.impl;

import br.ufal.ic.dao.ClientDAO;
import br.ufal.ic.dao.MaterialTypeDAO;
import br.ufal.ic.model.Client;
import br.ufal.ic.model.Employees;
import br.ufal.ic.model.MaterialType;
import br.ufal.ic.service.exception.CustomGenericException;
import br.ufal.ic.service.facades.MaterialTypeFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lucas on 21/07/15.
 */
public class MaterialTypeController implements MaterialTypeFacade {

    @Autowired
    private ClientDAO clientDAO;

    @Autowired
    MaterialTypeDAO materialTypeDAO;

    public MaterialType save(MaterialType materialType) throws Exception {
        return materialTypeDAO.save(materialType);
    }

    public MaterialType update(MaterialType materialType) throws Exception {
        return materialTypeDAO.update(materialType);
    }

    public ResponseEntity<Serializable> delete(Integer id) throws Exception {
        MaterialType materialType = materialTypeDAO.findById(id);
        // DELETE Material
        if (materialType != null){
            materialTypeDAO.delete(materialType);
            return new ResponseEntity<Serializable>("MaterialType deletado com sucesso", HttpStatus.OK);
        }

        return new ResponseEntity<Serializable>(new CustomGenericException(
                "" + HttpStatus.NOT_FOUND.value(),
                "Tipo material não encontrado."),
                HttpStatus.NOT_FOUND);

    }

    public List<MaterialType> findAll() {
        // FINDALL Material Type
        List<MaterialType> list = materialTypeDAO.findAll();

        return list;
    }

    public MaterialType findById(int id) throws Exception {
        MaterialType materialType = null;
        materialType = materialTypeDAO.findById(id);

        return materialType;
    }
}
