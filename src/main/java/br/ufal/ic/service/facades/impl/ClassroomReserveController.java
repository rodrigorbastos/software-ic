package br.ufal.ic.service.facades.impl;

import br.ufal.ic.dao.ClassroomDAO;
import br.ufal.ic.dao.ClassroomReserveDAO;
import br.ufal.ic.model.Classroom;
import br.ufal.ic.model.ClassroomReserve;
import br.ufal.ic.service.apis.logger.LoggerSender;
import br.ufal.ic.service.exception.CustomGenericException;
import br.ufal.ic.service.facades.ClassroomReserveFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by lucas on 21/07/15.
 */
public class ClassroomReserveController implements ClassroomReserveFacade {

    @Autowired
    ClassroomReserveDAO classroomReserveDAO;

    @Autowired
    LoggerSender logSender;

    private String serviceType = "Reserva de sala de aula";

    public ClassroomReserve save(ClassroomReserve classroomReserve) throws Exception {

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        logSender.writeLog(date, serviceType+": "+classroomReserve.getId(), "save", "admin");

        return classroomReserveDAO.save(classroomReserve);
    }

    public ClassroomReserve update(ClassroomReserve classroomReserve) throws Exception {

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        logSender.writeLog(date, serviceType+": "+classroomReserve.getId(), "update", "admin");

        return classroomReserveDAO.update(classroomReserve);
    }

    public ResponseEntity<Serializable> delete(Integer id) throws Exception {
        ClassroomReserve classroomReserve = classroomReserveDAO.findById(id);
        // DELETE Classroom
        if (classroomReserve != null){
            classroomReserveDAO.delete(classroomReserve);

            //LOGGER
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            String date = dateFormat.format(new Date());

            logSender.writeLog(date, serviceType+": "+classroomReserve.getId(), "delete", "admin");


            return new ResponseEntity<Serializable>("Reserva de sala de aula deletada com sucesso", HttpStatus.OK);
        }

        return new ResponseEntity<Serializable>(new CustomGenericException(
                "" + HttpStatus.NOT_FOUND.value(),
                "Reserva de sala de auala não encontrada."),
                HttpStatus.NOT_FOUND);
    }

    public List<ClassroomReserve> findAll()  {
        return classroomReserveDAO.findAll();
    }

    public ClassroomReserve findById(int id) throws Exception {
        ClassroomReserve classroomReserve = null;
        classroomReserve = classroomReserveDAO.findById(id);

        return classroomReserve;
    }
}
