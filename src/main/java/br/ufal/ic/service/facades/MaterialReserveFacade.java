package br.ufal.ic.service.facades;

import br.ufal.ic.model.Client;
import br.ufal.ic.model.MaterialReserve;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by rodrigo on 19/07/15.
 */
public interface MaterialReserveFacade {


    /*
	 * CRUD MATERIAL RESERVE
	 */

    public MaterialReserve save(MaterialReserve materialReserve) throws Exception;

    public MaterialReserve update(MaterialReserve materialReserve) throws Exception;

    public ResponseEntity<Serializable> delete(Integer materialReserve_id) throws Exception;


	/*
	 * QUERY
	 */

    public List<MaterialReserve> findAll();
    public ResponseEntity<Serializable> findById(int id) throws Exception;

}
