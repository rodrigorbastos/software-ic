package br.ufal.ic.service.facades.impl;

import br.ufal.ic.dao.ClientDAO;
import br.ufal.ic.dao.MaterialReserveDAO;
import br.ufal.ic.dao.StudentDAO;
import br.ufal.ic.model.Client;
import br.ufal.ic.model.Document;
import br.ufal.ic.model.MaterialReserve;
import br.ufal.ic.model.Student;
import br.ufal.ic.service.apis.logger.LoggerSender;
import br.ufal.ic.service.exception.CustomGenericException;
import br.ufal.ic.service.facades.DocFacade;
import br.ufal.ic.service.facades.StudentFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by rodrigo on 18/07/15.
 */
public class StudentController implements StudentFacade {

    @Autowired
    StudentDAO studentDAO;

    @Autowired
    DocFacade docFacade;

    @Autowired
    ClientDAO clientDAO;

    @Autowired
    LoggerSender logSender;

    @Autowired
    MaterialReserveDAO materialReserveDAO;

    private String serviceType = "Aluno";

    public Student save(Student student) throws Exception {
        // SAVE CLIENT
        Client client = student.getClient();
        client.setType_client("Aluno");
        client = clientDAO.save(student.getClient());
        student.setClient(client);
        // SAVE STUDENT
        student = studentDAO.save(student);

        //LOGGER
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = date_format.format(new Date());

        logSender.writeLog(date, serviceType+": "+student.getId(), "save", "admin");

        return student;
    }

    public Student update(Student student) throws Exception {
        Client client = student.getClient();
        client = clientDAO.update(client);

        // UPDATE STUDENT
        student.setClient(client);
        student = studentDAO.update(student);

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        logSender.writeLog(date, serviceType+": "+student.getId(), "update", "admin");

        return student;
    }

    public ResponseEntity<Serializable> delete(Integer student_id) throws Exception {
        Student student = studentDAO.findById(student_id);

        // GET CLIENT
        Client client = student.getClient();


        MaterialReserve mr = materialReserveDAO.findByIdClient(client);
        Document doc = docFacade.findByClient(client);

        if (mr == null && doc == null) {

            studentDAO.delete(student);
            clientDAO.delete(client);

            //LOGGER
            SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            String date = date_format.format(new Date());

            logSender.writeLog(date, serviceType+": "+student.getId(), "delete", "admin");

            return new ResponseEntity<Serializable>(
                    "{ \n\t\"sucesso\": \"Aluno deletado com sucesso\" \n}",
                    HttpStatus.OK);
        }

        return new ResponseEntity<Serializable>(new CustomGenericException(
                "" + HttpStatus.NOT_ACCEPTABLE.value(),
                "Aluno não deletado: dependências ativas"),
                HttpStatus.NOT_ACCEPTABLE);

    }

    public List<Student> findAll(){
        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        try {
            logSender.writeLog(date, serviceType+": list", "read", "admin");
        } catch (JMSException e) {
            e.printStackTrace();
        }

        return studentDAO.findAll();
    }

    public Student findById(int id) throws Exception {
        Student student = null;
        student = studentDAO.findById(id);

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        logSender.writeLog(date, serviceType+": list", "read", "admin");

        return student;


    }
}
