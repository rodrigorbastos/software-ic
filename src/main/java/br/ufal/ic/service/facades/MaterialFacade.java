package br.ufal.ic.service.facades;

import br.ufal.ic.model.Material;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by rodrigo on 19/07/15.
 */
public interface MaterialFacade {

    /*
	 * CRUD MATERIAL
	 */
    public Material save(Material material) throws Exception;

    public Material update(Material material) throws Exception;

    public ResponseEntity<Serializable> delete(Integer material_id) throws Exception;

    /*
     * AUXILIARES
     */
    public List<Material> findAll();

    public Material findById(int id) throws Exception;

}
