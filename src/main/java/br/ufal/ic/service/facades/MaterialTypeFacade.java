package br.ufal.ic.service.facades;

import br.ufal.ic.model.MaterialType;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lucas on 21/07/15.
 */
public interface MaterialTypeFacade {

    /*
	 * CRUD MATERIAL TYPE
	 */
    MaterialType save(MaterialType materialType) throws Exception;

    MaterialType update(MaterialType materialType) throws Exception;

    ResponseEntity<Serializable> delete(Integer id) throws Exception;




    /*
     * AUXILIARES
     */
    List<MaterialType> findAll();

    MaterialType findById(int id) throws Exception;
}
