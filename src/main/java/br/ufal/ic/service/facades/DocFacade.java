package br.ufal.ic.service.facades;

import br.ufal.ic.model.Client;
import br.ufal.ic.model.Document;

import javax.jms.JMSException;
import java.util.List;

/**
 * Created by rodrigo on 18/07/15.
 */
public interface DocFacade {

    public Document save( Document document) throws Exception;

    public Document update(Document document)throws Exception;

    void delete(Document document) throws Exception;

    public List<Document> findAll();

    public Document findById(int document_id)throws Exception;

    public Document findByClient(Client client)throws Exception;
}
