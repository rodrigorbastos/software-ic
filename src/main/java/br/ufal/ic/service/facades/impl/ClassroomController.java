package br.ufal.ic.service.facades.impl;

import br.ufal.ic.dao.ClassroomDAO;
import br.ufal.ic.dao.ClientDAO;
import br.ufal.ic.model.Classroom;
import br.ufal.ic.model.Client;
import br.ufal.ic.model.MaterialType;
import br.ufal.ic.service.apis.logger.LoggerSender;
import br.ufal.ic.service.exception.CustomGenericException;
import br.ufal.ic.service.facades.ClassroomFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by lucas on 21/07/15.
 */
public class ClassroomController implements ClassroomFacade{

    @Autowired
    private ClientDAO clientDAO;

    @Autowired
    ClassroomDAO classroomDAO;

    @Autowired
    LoggerSender logSender;

    private String serviceType = "Sala de aula";

    public Classroom save(Classroom classroom) throws Exception {

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        logSender.writeLog(date, serviceType+": "+classroom.getId(), "save", "admin");

        return classroomDAO.save(classroom);
    }

    public Classroom update(Classroom classroom) throws Exception {

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        logSender.writeLog(date, serviceType+": "+classroom.getId(), "update", "admin");

        return classroomDAO.update(classroom);
    }

    public ResponseEntity<Serializable> delete(Integer id) throws Exception {
        Classroom classroom = classroomDAO.findById(id);
        // DELETE Classroom
        if (classroom != null){
            classroomDAO.delete(classroom);

            //LOGGER
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            String date = dateFormat.format(new Date());

            logSender.writeLog(date, serviceType+": "+classroom.getId(), "delete", "admin");

            return new ResponseEntity<Serializable>("Sala de aula deletada com sucesso", HttpStatus.OK);
        }

        return new ResponseEntity<Serializable>(new CustomGenericException(
                "" + HttpStatus.NOT_FOUND.value(),
                "Sala de aula não encontrada."),
                HttpStatus.NOT_FOUND);
    }

    public List<Classroom> findAll() {
        // FINDALL Material
        List<Classroom> list = classroomDAO.findAll();

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        try {
            logSender.writeLog(date, serviceType+": list", "read", "admin");
        } catch (JMSException e) {
            e.printStackTrace();
        }

        return list;
    }

    public Classroom findById(int id) throws Exception {
        Classroom classroom = null;
        classroom = classroomDAO.findById(id);

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        logSender.writeLog(date, serviceType+": list", "read", "admin");

        return classroom;
    }
}
