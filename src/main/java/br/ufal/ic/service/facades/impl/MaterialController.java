package br.ufal.ic.service.facades.impl;

import br.ufal.ic.dao.MaterialDAO;
import br.ufal.ic.model.Material;
import br.ufal.ic.model.Student;
import br.ufal.ic.service.apis.logger.LoggerSender;
import br.ufal.ic.service.exception.CustomGenericException;
import br.ufal.ic.service.facades.MaterialFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by rodrigo on 19/07/15.
 */
public class MaterialController implements MaterialFacade {

    @Autowired
    MaterialDAO materialDAO;

    @Autowired
    LoggerSender logSender;

    private String serviceType = "Material";

    public Material save(Material material) throws Exception {

        //LOGGER
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = date_format.format(new Date());

        logSender.writeLog(date, serviceType+": "+material.getId(), "save", "admin");

        return materialDAO.save(material);
    }

    public Material update(Material material) throws Exception {
        material = materialDAO.update(material);

        //LOGGER
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = date_format.format(new Date());

        logSender.writeLog(date, serviceType+": "+material.getId(), "update", "admin");

        return material;
    }

    public ResponseEntity<Serializable> delete(Integer material_id) throws Exception {
        Material material = materialDAO.findById(material_id);
        // DELETE Material
        if (material != null){
            materialDAO.delete(material);

            //LOGGER
            SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            String date = date_format.format(new Date());

            logSender.writeLog(date, serviceType+": "+material.getId(), "delete", "admin");

            return new ResponseEntity<Serializable>("Material deletado com sucesso", HttpStatus.OK);
        }

        return new ResponseEntity<Serializable>(new CustomGenericException(
                "" + HttpStatus.NOT_ACCEPTABLE.value(),
                "Material não deletado: referência não encontrada"),
                HttpStatus.NOT_ACCEPTABLE);
    }

    public List<Material> findAll(){
        // FINDALL Material
        List<Material> list = materialDAO.findAll();

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        try {
            logSender.writeLog(date, serviceType+": list", "read", "admin");
        } catch (JMSException e) {
            e.printStackTrace();
        }

        return list;
    }

    public Material findById(int id) throws Exception {
        Material material = null;
        material = materialDAO.findById(id);

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        logSender.writeLog(date, serviceType+": list", "read", "admin");

        return material;

    }
}
