package br.ufal.ic.service.facades.impl;

import br.ufal.ic.dao.ClientDAO;
import br.ufal.ic.dao.DocumentDAO;
import br.ufal.ic.model.Client;
import br.ufal.ic.model.Document;
import br.ufal.ic.service.apis.TrelloSender;
import br.ufal.ic.service.apis.logger.LoggerSender;
import br.ufal.ic.service.facades.DocFacade;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.JMSException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by rodrigo on 18/07/15.
 */
public class DocController implements DocFacade{

    @Autowired
    DocumentDAO documentDAO;

    @Autowired
    ClientDAO clientDAO;

    @Autowired
    private TrelloSender trelloSender;

    @Autowired
    LoggerSender logSender;

    private String serviceType = "Documento";

    public Document save(Document document) throws Exception {


        Date date = new Date();

//        Client client = clientDAO.findById(document.getClient().getId());

        document.setStartDate(date);
        document = documentDAO.save(document);

        Client client = document.getClient();
        //Create Card Trello
        String name  = "Criar "+document.getDocType()+" para o "+client.getType_client()+" "+client.getName();
        String desc = document.getDescription()+" ---- Dados do solicitante: "+client.toString();

        trelloSender.createActivity(name, desc);

        //LOGGER
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String dateLogger = date_format.format(date);

        logSender.writeLog(dateLogger, serviceType+": "+document.getId(), "save", "admin");

        return document;
    }

    public Document update(Document document) throws Exception {

        document = documentDAO.update(document);

        //LOGGER
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = date_format.format(new Date());

        logSender.writeLog(date, serviceType+": "+document.getId(), "update", "admin");

        return document;
    }

    public void delete(Document document) throws Exception {
        //DELETE Document
        documentDAO.delete(document);

        //LOGGER
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = date_format.format(new Date());

        logSender.writeLog(date, serviceType+": "+document.getId(), "delete", "admin");
    }

    public List<Document> findAll() {
        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        try {
            logSender.writeLog(date, serviceType+": list", "read", "admin");
        } catch (JMSException e) {
            e.printStackTrace();
        }

        return documentDAO.findAll();
    }

    public Document findById(int document_id) throws Exception {

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        logSender.writeLog(date, serviceType+": list", "read", "admin");

        return documentDAO.findById(document_id);
    }

    public Document findByClient(Client client) throws Exception {

        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        logSender.writeLog(date, serviceType+": list", "read", "admin");

        return documentDAO.findByClient(client);
    }


}
