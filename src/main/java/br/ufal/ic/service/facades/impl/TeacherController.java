package br.ufal.ic.service.facades.impl;

import br.ufal.ic.dao.*;
import br.ufal.ic.model.Client;
import br.ufal.ic.model.Document;
import br.ufal.ic.model.MaterialReserve;
import br.ufal.ic.model.Teacher;
import br.ufal.ic.service.apis.logger.LoggerSender;
import br.ufal.ic.service.exception.CustomGenericException;
import br.ufal.ic.service.facades.TeacherFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by rodrigo on 19/07/15.
 */
public class TeacherController implements TeacherFacade{

    @Autowired
    private TeacherDAO teacherDAO;

    @Autowired
    private ClientDAO clientDAO;

    @Autowired
    LoggerSender logSender;

    @Autowired
    ClassroomReserveDAO classroomReserveDAO;
    @Autowired
    MaterialReserveDAO materialReserveDAO;
    @Autowired
    DocumentDAO documentDAO;

    private String serviceType = "Professor";

    public Teacher save(Teacher teacher) throws Exception {
        System.out.println(teacher.toString());
        Client client = clientDAO.save(teacher.getClient());
        teacher.setClient(client);
        // SAVE teacher
        teacher = teacherDAO.save(teacher);

        //LOGGER
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = date_format.format(new Date());

        logSender.writeLog(date, serviceType+": "+teacher.getId(), "save", "admin");

        return  teacher;
    }

    public Teacher update(Teacher teacher) throws Exception {
        Client client = teacher.getClient();

        client = clientDAO.update(client);

        teacher.setClient(client);
        teacher = teacherDAO.update(teacher);

        //LOGGER
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = date_format.format(new Date());

        logSender.writeLog(date, serviceType+": "+teacher.getId(), "update", "admin");

        return teacher;
    }

    public ResponseEntity<Serializable> delete(Integer teacher_id) throws Exception {
        // GET Teacher
        Teacher teacher = teacherDAO.findById(teacher_id);

        // GET CLIENT
        Client client = teacher.getClient();

        MaterialReserve mr = materialReserveDAO.findByIdClient(client);
        Document doc = documentDAO.findByClient(client);

        if (mr == null && doc == null) {
            teacherDAO.delete(teacher);
            clientDAO.delete(client);

            //LOGGER
            SimpleDateFormat date_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            String date = date_format.format(new Date());

            logSender.writeLog(date, serviceType+": "+teacher.getId(), "delete", "admin");


            return new ResponseEntity<Serializable>(
                    "{ \n\t\"sucesso\": \"Professor deletado com sucesso\" \n}",
                    HttpStatus.OK);
        }

        return new ResponseEntity<Serializable>(new CustomGenericException(
                "" + HttpStatus.NOT_ACCEPTABLE.value(),
                "Professor não deletado: dependências ativas"),HttpStatus.NOT_ACCEPTABLE);
    }

    public List<Teacher> findAll() {
        //LOGGER
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String date = dateFormat.format(new Date());

        try {
            logSender.writeLog(date, serviceType+": list", "read", "admin");
        } catch (JMSException e) {
            e.printStackTrace();
        }

        return teacherDAO.findAll();
    }

    public ResponseEntity<Serializable> findById(int teacher_id) throws Exception {
        Teacher teacher = teacherDAO.findById(teacher_id);
        if (teacher != null) {

            //LOGGER
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            String date = dateFormat.format(new Date());

            logSender.writeLog(date, serviceType+": list", "read", "admin");

            return new ResponseEntity<Serializable>(teacher, HttpStatus.OK);
        }

        return new ResponseEntity<Serializable>(new CustomGenericException(
                "" + HttpStatus.NOT_FOUND.value(),
                "Professor não encontrado"), HttpStatus.NOT_FOUND);
    }
}
