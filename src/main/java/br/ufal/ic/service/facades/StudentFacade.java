package br.ufal.ic.service.facades;

import br.ufal.ic.model.Student;
import org.springframework.http.ResponseEntity;

import javax.jms.JMSException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by rodrigo on 18/07/15.
 */
public interface StudentFacade {
    /*
	 *CRUD STUDENT
	 */
    public Student save(Student student) throws Exception;

    public Student update(Student student) throws Exception;

    public ResponseEntity<Serializable> delete(Integer student_id) throws Exception;

	/*
	 * AUXILIARES
	 */

    public List<Student> findAll();

    public Student findById(int id) throws Exception;
}
