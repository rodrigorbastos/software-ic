/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufal.ic.service.controller;

import br.ufal.ic.dao.ClassroomDAO;
import br.ufal.ic.model.Classroom;
import br.ufal.ic.service.exception.CustomGenericException;
import java.io.Serializable;
import java.util.List;

import br.ufal.ic.service.facades.ClassroomFacade;
import org.hibernate.exception.ConstraintViolationException;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Jario
 */
@Controller
@RequestMapping("/api/classroom")
public class ClassroomWS {
    
    @Autowired
    ClassroomDAO classroomDAO;

    @Autowired
    private ClassroomFacade classroomFacade;

    @ExceptionHandler(CustomGenericException.class)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<Serializable> classroomSave(@RequestBody Classroom classroom) {

        try {
            //classroom = classroomDAO.save(classroom);
            classroom = classroomFacade.save(classroom);
            return new ResponseEntity<Serializable>(classroom, HttpStatus.OK);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "Classroom não cadastro: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(HttpStatus.BAD_REQUEST);
        }

    }

    @ExceptionHandler(CustomGenericException.class)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<Serializable> classroomUpdate(@RequestBody Classroom classroom) {

        // UPDATE Classroom
        try {
            //classroom = classroomDAO.update(classroom);
            classroom = classroomFacade.update(classroom);
            return new ResponseEntity<Serializable>(classroom, HttpStatus.OK);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "Classroom não atualizado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "Classroom não encontrado"), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    ResponseEntity<Serializable> classroomDelete(@PathVariable("id") Integer id) {
        //get Classroom
        try {
            return classroomFacade.delete(id);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "Classroom não deletado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "Classroom não encontrado"), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public @ResponseBody
    List<Classroom> classroomFindAll() {

        // FINDALL Classroom
        return classroomFacade.findAll();
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Serializable> classroomGet(
            @RequestParam(value = "id", required = true) Integer id) {

        // FINDBYID Classroom
        Classroom classroom;
        try {
            classroom = classroomFacade.findById(id);
            return new ResponseEntity<Serializable>(classroom, HttpStatus.OK);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "Classroom não deletado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "Classroom não encontrado"), HttpStatus.BAD_REQUEST);
        }

    }
}
