/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufal.ic.service.controller;

import br.ufal.ic.dao.ClassroomReserveDAO;
import br.ufal.ic.model.ClassroomReserve;
import br.ufal.ic.model.Client;
import br.ufal.ic.service.exception.CustomGenericException;
import java.io.Serializable;
import java.util.List;

import br.ufal.ic.service.facades.ClassroomReserveFacade;
import org.hibernate.exception.ConstraintViolationException;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.jms.JMSException;

/**
 *
 * @author Jario
 */
@Controller
@RequestMapping("/api/classroom_reserve")
public class ClassroomReserveWS {
    
    @Autowired
    ClassroomReserveDAO classroomReserveDAO;

    @Autowired
    private ClassroomReserveFacade classroomReserveFacade;

    @ExceptionHandler(CustomGenericException.class)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<Serializable> classroomReserveSave(@RequestBody ClassroomReserve classroomReserve) {

        try {
        	
            //classroomReserve = classroomReserveDAO.save(classroomReserve);
            classroomReserve = classroomReserveFacade.save(classroomReserve);
            return new ResponseEntity<Serializable>(classroomReserve, HttpStatus.OK);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "ClassroomReserve não cadastro: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(HttpStatus.BAD_REQUEST);
        }

    }

    @ExceptionHandler(CustomGenericException.class)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<Serializable> classroomReserveUpdate(@RequestBody ClassroomReserve classroomReserve) {

        // UPDATE ClassroomReserve
        try {
            //classroomReserve = classroomReserveDAO.update(classroomReserve);
            classroomReserve = classroomReserveFacade.update(classroomReserve);
            return new ResponseEntity<Serializable>(classroomReserve, HttpStatus.OK);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "ClassroomReserve não atualizado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "ClassroomReserve não encontrado"), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    ResponseEntity<Serializable> classroomReserveDelete(@PathVariable("id") Integer id) {
        //get ClassroomReserve
        try {
            return classroomReserveFacade.delete(id);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "ClassroomReserve não deletado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "ClassroomReserve não encontrado"), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public @ResponseBody
    List<ClassroomReserve> classroomReserveFindAll() {
        // FINDALL Classroom Reserve
        return classroomReserveFacade.findAll();
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Serializable> classroomReserveGet(
            @RequestParam(value = "id", required = true) Integer id) {
        ClassroomReserve classroomReserve;
        try {
            classroomReserve = classroomReserveFacade.findById(id);
            return new ResponseEntity<Serializable>(classroomReserve, HttpStatus.OK);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "ClassroomReserve não deletado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "ClassroomReserve não encontrado"), HttpStatus.BAD_REQUEST);
        }

    }
    
    @RequestMapping(value="/findbyclient",method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Serializable> classroomReserveGetByClient(
            @RequestBody Client client) {

        // FINDBYID ClassroomReserve
        ClassroomReserve classroomReserve;
        try {
            classroomReserve = classroomReserveDAO.findByIdClient(client);
            return new ResponseEntity<Serializable>(classroomReserve, HttpStatus.OK);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "ClassroomReserve não deletado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "ClassroomReserve não encontrado"), HttpStatus.BAD_REQUEST);
        }

    }
}
