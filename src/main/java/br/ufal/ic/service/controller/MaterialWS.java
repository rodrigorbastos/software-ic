/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufal.ic.service.controller;

import java.io.Serializable;
import java.util.List;

import br.ufal.ic.model.Student;
import br.ufal.ic.service.facades.MaterialFacade;
import org.hibernate.exception.ConstraintViolationException;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.ufal.ic.dao.MaterialDAO;
import br.ufal.ic.model.Material;
import br.ufal.ic.service.exception.CustomGenericException;

/**
 * 
 * @author Jario
 */
@Controller
@RequestMapping("/api/material")
public class MaterialWS {

//	@Autowired
//	MaterialDAO materialDAO;

	@Autowired
	MaterialFacade materialFacade;

	@ExceptionHandler(CustomGenericException.class)
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Serializable> materialSave(@RequestBody Material material){

		try {
			material = materialFacade.save(material);
			return new ResponseEntity<Serializable>(material,HttpStatus.OK);
		} catch (PSQLException e) {
			return new ResponseEntity<Serializable>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}catch (ConstraintViolationException e){
			return new ResponseEntity<Serializable>(new CustomGenericException(""+HttpStatus.CONFLICT.value(), "Material não cadastro: conflito nos dados"),HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(HttpStatus.BAD_REQUEST);
		}
		
		

	}

	@ExceptionHandler(CustomGenericException.class)
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Serializable> materialUpdate(@RequestBody Material material) {

		// UPDATE Material
		try {
			material = materialFacade.update(material);
			return new ResponseEntity<Serializable>(material,HttpStatus.OK);
		} catch (PSQLException e) {
			return new ResponseEntity<Serializable>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}catch (ConstraintViolationException e){
			return new ResponseEntity<Serializable>(new CustomGenericException(""+HttpStatus.CONFLICT.value(), "Material não atualizado: conflito nos dados"),HttpStatus.CONFLICT);
		}catch (Exception e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(""+HttpStatus.BAD_REQUEST.value(), "Material não encontrado"),HttpStatus.BAD_REQUEST);
		} 
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Serializable> materialDelete(@PathVariable("id") Integer id) {
		//get Material
		try{
			return materialFacade.delete(id);
		} catch (PSQLException e) {
			return new ResponseEntity<Serializable>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}catch (ConstraintViolationException e){
			return new ResponseEntity<Serializable>(new CustomGenericException(""+HttpStatus.CONFLICT.value(), "Material não deletado: conflito nos dados"),HttpStatus.CONFLICT);
		}catch (Exception e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(""+HttpStatus.BAD_REQUEST.value(), "Material não encontrado"),HttpStatus.BAD_REQUEST);
		} 
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public  @ResponseBody List<Material> materialFindAll() {
		return materialFacade.findAll();
	}

	@RequestMapping(method = RequestMethod.GET)
	public  @ResponseBody ResponseEntity<Serializable> materialGet(
			@RequestParam(value = "id", required = true) Integer id) {

		// FINDBYID Material
		try {
			Material material = materialFacade.findById(id);
			if (material != null) {
				return new ResponseEntity<Serializable>(material, HttpStatus.OK);
			}
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.NOT_FOUND.value(), "Material não encontrado"),
					HttpStatus.NOT_FOUND);
		} catch (PSQLException e) {
			return new ResponseEntity<Serializable>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}catch (ConstraintViolationException e){
			return new ResponseEntity<Serializable>(new CustomGenericException(""+HttpStatus.CONFLICT.value(), "Material não deletado: conflito nos dados"),HttpStatus.CONFLICT);
		}catch (Exception e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(""+HttpStatus.BAD_REQUEST.value(), "Material não encontrado"),HttpStatus.BAD_REQUEST);
		} 

		
	}
}
