/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufal.ic.service.controller;

import br.ufal.ic.dao.MaterialReserveDAO;
import br.ufal.ic.model.MaterialReserve;
import br.ufal.ic.service.exception.CustomGenericException;
import java.io.Serializable;
import java.util.List;

import br.ufal.ic.service.facades.MaterialReserveFacade;
import org.hibernate.exception.ConstraintViolationException;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Jario
 */
@Controller
@RequestMapping("/api/material_reserve")
public class MaterialReserveWS {


	@Autowired
	MaterialReserveFacade materialReserveFacade;

//	@Autowired
//	MaterialReserveDAO materialReserveDAO;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody
	ResponseEntity<Serializable> materialReserveSave(
			@RequestBody MaterialReserve materialReserve) {

		try {
			materialReserve = materialReserveFacade.save(materialReserve);
			return new ResponseEntity<Serializable>(materialReserve,
					HttpStatus.OK);
		} catch (PSQLException e) {
			return new ResponseEntity<Serializable>(e.getMessage(),
					HttpStatus.BAD_REQUEST);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"MaterialReserve não cadastro"), HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public @ResponseBody
	ResponseEntity<Serializable> materialReserveUpdate(
			@RequestBody MaterialReserve materialReserve) {

		// UPDATE Material
		try {
			materialReserve = materialReserveFacade.update(materialReserve);
			return new ResponseEntity<Serializable>(materialReserve,
					HttpStatus.OK);
		} catch (PSQLException e) {
			return new ResponseEntity<Serializable>(e.getMessage(),
					HttpStatus.BAD_GATEWAY);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"MaterialReserve não atualizado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.BAD_REQUEST.value(),
					"MaterialReserve não encontrado"), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody
	ResponseEntity<Serializable> materialReserveDelete(
			@PathVariable("id") Integer id) {

		try {
			return materialReserveFacade.delete(id);
		} catch (PSQLException e) {
			return new ResponseEntity<Serializable>(e.getMessage(),
					HttpStatus.BAD_REQUEST);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"MaterialReserve não deletado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.BAD_REQUEST.value(),
					"MaterialReserve não encontrado"), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody
	List<MaterialReserve> materialReserveList() {
		return materialReserveFacade.findAll();
	}

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	ResponseEntity<Serializable> materialReserveGet(
			@RequestParam(value = "id", required = true) Integer id) {


		try {
			return materialReserveFacade.findById(id);
		} catch (PSQLException e) {
			return new ResponseEntity<Serializable>(e.getMessage(),
					HttpStatus.BAD_GATEWAY);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"MaterialReserve não encontrado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.BAD_REQUEST.value(),
					"MaterialReserve não encontrado"), HttpStatus.BAD_REQUEST);
		}

	}
}
