package br.ufal.ic.service.controller;

import java.io.Serializable;
import java.util.List;

import br.ufal.ic.service.facades.TeacherFacade;
import org.hibernate.exception.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import br.ufal.ic.model.Teacher;

import br.ufal.ic.service.exception.CustomGenericException;

@Controller
@RequestMapping(value = "/api/teacher")
public class TeacherWS {

	@Autowired
	private TeacherFacade teacherFacade;
	


	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody
	ResponseEntity<Serializable> teacherSave(@RequestBody Teacher teacher) {
		try {
			// SAVE CLIENT
			teacher = teacherFacade.save(teacher);

			return new ResponseEntity<Serializable>(teacher, HttpStatus.OK);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"Professor não cadastrado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public @ResponseBody
	ResponseEntity<Serializable> teacherUpdate(@RequestBody Teacher teacher) {
		try {

			teacher = teacherFacade.update(teacher);

			return new ResponseEntity<Serializable>(teacher, HttpStatus.OK);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"Professor não atualizado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody
	ResponseEntity<Serializable> teacherDelete(@PathVariable("id") Integer id) {

		try {
			return teacherFacade.delete(id);
			
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"Professor não deletado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.BAD_REQUEST.value(),
					"Professor não encontrado"), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody
	List<Teacher> teacherList() {

		return teacherFacade.findAll();
	}

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	ResponseEntity<Serializable> teacherGet(
			@RequestParam(value = "id", required = true) int id) {
		try {
			return teacherFacade.findById(id);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"Aluno não encontrado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(
					new CustomGenericException(""
							+ HttpStatus.BAD_REQUEST.value(),
							"Aluno não encontrado"), HttpStatus.BAD_REQUEST);
		}

	}

}
