/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufal.ic.service.controller;

import br.ufal.ic.dao.MaterialTypeDAO;
import br.ufal.ic.model.MaterialType;
import br.ufal.ic.service.exception.CustomGenericException;
import java.io.Serializable;
import java.util.List;

import br.ufal.ic.service.facades.MaterialTypeFacade;
import org.hibernate.exception.ConstraintViolationException;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Jario
 */
@Controller
@RequestMapping("/api/material_type")
public class MaterialTypeWS {
    
    @Autowired
    MaterialTypeDAO materialTypeDAO;

    @Autowired
    private MaterialTypeFacade materialTypeFacade;

    @ExceptionHandler(CustomGenericException.class)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<Serializable> materialTypeSave(@RequestBody MaterialType materialType) {

        try {
            materialType = materialTypeFacade.save(materialType);
            return new ResponseEntity<Serializable>(materialType, HttpStatus.OK);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "MaterialType não cadastro"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(HttpStatus.BAD_REQUEST);
        }

    }

    @ExceptionHandler(CustomGenericException.class)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<Serializable> materialTypeUpdate(@RequestBody MaterialType materialType) {

        // UPDATE Material
        try {
            //materialType = materialTypeDAO.update(materialType);
            materialType = materialTypeFacade.update(materialType);
            return new ResponseEntity<Serializable>(materialType, HttpStatus.OK);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "MaterialType não atualizado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "MaterialType não encontrado"), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    ResponseEntity<Serializable> materialTypeDelete(@PathVariable("id") Integer id) {
        //get Material
        try {
           return materialTypeFacade.delete(id);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "MaterialType não deletado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "MaterialType não encontrado"), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public @ResponseBody
    List<MaterialType> materialFindAll() {

        // FINDALL Material
        List<MaterialType> list = materialTypeFacade.findAll();

        return list;
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Serializable> materialTypeGet(
            @RequestParam(value = "id", required = true) Integer id) {

        // FINDBYID Material
        MaterialType materialType;
        try {
            materialType = materialTypeFacade.findById(id);
            return new ResponseEntity<Serializable>(materialType, HttpStatus.OK);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "MaterialType não deletado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "MaterialType não encontrado"), HttpStatus.BAD_REQUEST);
        }

    }
}
