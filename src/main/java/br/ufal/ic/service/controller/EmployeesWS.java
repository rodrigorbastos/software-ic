package br.ufal.ic.service.controller;

import java.io.Serializable;
import java.util.List;

import br.ufal.ic.service.facades.EmployeesFacade;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.ufal.ic.dao.ClassroomReserveDAO;
import br.ufal.ic.dao.ClientDAO;
import br.ufal.ic.dao.DocumentDAO;
import br.ufal.ic.dao.EmployeesDAO;
import br.ufal.ic.dao.MaterialReserveDAO;
import br.ufal.ic.model.ClassroomReserve;
import br.ufal.ic.model.Client;
import br.ufal.ic.model.Document;
import br.ufal.ic.model.Employees;
import br.ufal.ic.model.MaterialReserve;
import br.ufal.ic.service.exception.CustomGenericException;

@Controller
@RequestMapping("api/employees")
public class EmployeesWS {

	@Autowired
	private EmployeesFacade employeesFacade;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody
	ResponseEntity<Serializable> employeesSave(@RequestBody Employees employees) {
		try {
			// SAVE EMPLOYEES
			employees = employeesFacade.save(employees);
			return new ResponseEntity<Serializable>(employees, HttpStatus.OK);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"Servidor não cadastrado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public @ResponseBody
	ResponseEntity<Serializable> employeesUpdate(
			@RequestBody Employees employees) {
		try {

			employees = employeesFacade.update(employees);

			return new ResponseEntity<Serializable>(employees, HttpStatus.OK);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"Servidor não atualizado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody
	ResponseEntity<Serializable> employeesDelete(@PathVariable("id") Integer id) {
		try {
			return employeesFacade.delete(id);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"Servidor não deletado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.BAD_REQUEST.value(),
					"Servidor não encontrado"), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody
	List<Employees> employeesList() {

		return employeesFacade.findAll();
	}

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	ResponseEntity<Serializable> employeesGet(
			@RequestParam(value = "id", required = true) int id) {
		Employees employees;
		try {
			employees = employeesFacade.findById(id);
			if (employees != null) {
				return new ResponseEntity<Serializable>(employees,
						HttpStatus.OK);
			}

			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.NOT_FOUND.value(),
					"Servidor não encontrado"), HttpStatus.NOT_FOUND);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"Servidor não encontrado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.BAD_REQUEST.value(),
					"Servidor não encontrado"), HttpStatus.BAD_REQUEST);
		}

	}

}
