/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufal.ic.service.controller;

import br.ufal.ic.model.Client;
import br.ufal.ic.dao.ClientDAO;
import br.ufal.ic.model.Document;
import br.ufal.ic.service.facades.DocFacade;
import br.ufal.ic.service.exception.CustomGenericException;

import java.util.List;
import java.io.Serializable;

import org.postgresql.util.PSQLException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 *
 * @author Rodrigo
 */
@Controller
@RequestMapping("/api/document")
public class DocumetWS {

    @Autowired
    DocFacade docFacade;

    //ToDo - trocar esse objeto pelo clientFacade
    @Autowired
    ClientDAO clientDAO;

    @ExceptionHandler(CustomGenericException.class)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<Serializable> documentSave(@RequestBody Document document) {

        try {
            Client client = clientDAO.findById(document.getClient().getId());
            if (client != null ){
                document.setClient(client);
                document = docFacade.save(document);
                return new ResponseEntity<Serializable>(document, HttpStatus.OK);
            }
            return new ResponseEntity<Serializable>("Documento não cadastro: Cliente solicitante nao existe", HttpStatus.BAD_REQUEST);

        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "Document não cadastro: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(HttpStatus.BAD_REQUEST);
        }

    }

    @ExceptionHandler(CustomGenericException.class)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<Serializable> documentUpdate(@RequestBody Document document) {

        // UPDATE Document
        try {
            document = docFacade.update(document);
            return new ResponseEntity<Serializable>(document, HttpStatus.OK);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "Document não atualizado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "Document não encontrado"), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    ResponseEntity<Serializable> documentDelete(@PathVariable("id") Integer id) {
        try {
            Document document = docFacade.findById(id);
            docFacade.delete(document);
            return new ResponseEntity<Serializable>("Document deletado com sucesso", HttpStatus.OK);
        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "Document não deletado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "Document não encontrado"), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public @ResponseBody
    List<Document> documentFindAll() {

        // FINDALL Document
        List<Document> list = docFacade.findAll();
        return list;
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Serializable> documentGet(
            @RequestParam(value = "id", required = true) Integer id) {

        // FINDBYID Document
        Document document;
        try {

            document = docFacade.findById(id);

            if (document != null){
                return new ResponseEntity<Serializable>(document, HttpStatus.OK);
            }
            return new ResponseEntity<Serializable>(new CustomGenericException(
                    "" + HttpStatus.NOT_FOUND.value(), "Documento não encontrado"),
                    HttpStatus.NOT_FOUND);

        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "Document não deletado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "Document não encontrado"), HttpStatus.BAD_REQUEST);
        }

    }
    
    @RequestMapping(value="/findbyclient",method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Serializable> documentGetByClient(
            @RequestBody Client client) {
        Document document;
        try {
            document = docFacade.findByClient(client);

            if (document != null){
                return new ResponseEntity<Serializable>(document, HttpStatus.OK);
            }
            return new ResponseEntity<Serializable>(new CustomGenericException(
                    "" + HttpStatus.NOT_FOUND.value(), "Documento não encontrado"),
                    HttpStatus.NOT_FOUND);

        } catch (PSQLException e) {
            return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.CONFLICT.value(), "Document não deletado: conflito nos dados"), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<Serializable>(new CustomGenericException("" + HttpStatus.BAD_REQUEST.value(), "Document não encontrado"), HttpStatus.BAD_REQUEST);
        }

    }
}
