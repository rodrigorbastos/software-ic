package br.ufal.ic.service.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import br.ufal.ic.service.facades.StudentFacade;
import org.hibernate.HibernateException;
import org.hibernate.exception.ConstraintViolationException;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.ufal.ic.dao.ClassroomReserveDAO;
import br.ufal.ic.dao.ClientDAO;
import br.ufal.ic.dao.DocumentDAO;
import br.ufal.ic.dao.MaterialReserveDAO;
import br.ufal.ic.dao.StudentDAO;
import br.ufal.ic.model.ClassroomReserve;
import br.ufal.ic.model.Client;
import br.ufal.ic.model.Document;
import br.ufal.ic.model.MaterialReserve;
import br.ufal.ic.model.Student;
import br.ufal.ic.service.exception.CustomGenericException;
import sun.security.validator.ValidatorException;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

@Controller
@RequestMapping("/api/student")
public class StudentWS {

	@Autowired
	StudentFacade studentFacade;

	@Autowired
	private static Validator validator;


	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody
	ResponseEntity<Serializable> studentSave(@RequestBody Student student) {
		try {
			// SAVE CLIENT

			student = studentFacade.save(student);
			return new ResponseEntity<Serializable>(student, HttpStatus.OK);
//			return new ResponseEntity<Serializable>("Dados não válido", HttpStatus.NOT_ACCEPTABLE);


		} catch (PSQLException e) {
			return new ResponseEntity<Serializable>(e.getMessage(),
					HttpStatus.BAD_GATEWAY);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"Aluno não cadastrado: conflito nos dados"),
					HttpStatus.CONFLICT);
		}catch(ValidatorException e){
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"Aluno não cadastrado: "+e.getMessage()),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>("Dados não validos: "+e.getCause(), HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public @ResponseBody
	ResponseEntity<Serializable> studentUpdate(@RequestBody Student student) {
		try {

			student = studentFacade.update(student);

			return new ResponseEntity<Serializable>(student, HttpStatus.OK);
		} catch (PSQLException e) {
			return new ResponseEntity<Serializable>(e.getMessage(),
					HttpStatus.BAD_REQUEST);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"Aluno não atualizado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody
	ResponseEntity<Serializable> studentDelete(@PathVariable("id") Integer id) {
		try {
			return studentFacade.delete(id);
		} catch (PSQLException e) {
			return new ResponseEntity<Serializable>(e.getMessage(),
					HttpStatus.BAD_GATEWAY);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"Aluno não deletado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(
					new CustomGenericException(""
							+ HttpStatus.BAD_REQUEST.value(),
							"Aluno não encontrado"), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody
	List<Student> studentList() {

		return studentFacade.findAll();
	}

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	ResponseEntity<Serializable> studentGet(
			@RequestParam(value = "id", required = true) int id) {

		try {

			Student student = studentFacade.findById(id);
			if (student != null) {
				return new ResponseEntity<Serializable>(student, HttpStatus.OK);
			}
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.NOT_FOUND.value(), "Aluno não encontrado"),
					HttpStatus.NOT_FOUND);

		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Serializable>(new CustomGenericException(
					"" + HttpStatus.CONFLICT.value(),
					"Aluno não encontrado: conflito nos dados"),
					HttpStatus.CONFLICT);
		} catch (HibernateException e) {
			return new ResponseEntity<Serializable>(e.getMessage(),
					HttpStatus.BAD_GATEWAY);
		} catch (Exception e) {
			return new ResponseEntity<Serializable>(
					new CustomGenericException(""
							+ HttpStatus.BAD_REQUEST.value(),
							"Aluno não encontrado"), HttpStatus.BAD_REQUEST);
		}

	}

}
