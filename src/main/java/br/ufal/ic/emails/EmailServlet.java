package br.ufal.ic.emails;

import com.sun.net.httpserver.HttpServer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by rodrigo on 20/06/15.
 */
public class EmailServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());

        EmailBO emailBO = (EmailBO)springContext.getBean("emailBO");

        emailBO.enviarEmail();

        response.sendRedirect("sucesso.jsp");
    }

}
