package br.ufal.ic.emails;

/**
 * Created by rodrigo on 14/07/15.
 */

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

public class ListenerFilaEmail implements MessageListener {

    public void onMessage(Message message) {
        MapMessage mapMessage = (MapMessage) message;

        try {
            // obtem dados da mensagem
            String to = mapMessage.getString("to");
            String subject = mapMessage.getString("subject");
            String content = mapMessage.getString("content");

            // envia email

           /* TODO: aqui voce chama sua rotina de envio de email passando
           *  os dados recebidos pela mensagem
           */
            try {
                Thread.sleep(5000); // simula tempo de envio de e-logger
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("E-logger enviado.");

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
