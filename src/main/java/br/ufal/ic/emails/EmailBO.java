package br.ufal.ic.emails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by rodrigo on 20/06/15.
 */

@Service("emailBO")
public class EmailBO {

    @Autowired
    private ProdutorFilaEmail produtorFilaEmail;

    public void enviarEmail(){
        String to ="rrb@ic.ufal.br";
        String subject ="Teste Spring JMS";
        String content = "Este é um teste do Spring JMS";

        produtorFilaEmail.enviarMensagem(to, subject, content);
    }
}
