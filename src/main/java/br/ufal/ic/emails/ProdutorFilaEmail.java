package br.ufal.ic.emails;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.*;

/**
 * Created by rodrigo on 20/06/15.
 */
public class ProdutorFilaEmail {

    private JmsTemplate jmsTemplate;
    private Destination destination;

    public void setJmsTemplate(JmsTemplate jmsTemplate){
        this.jmsTemplate = jmsTemplate;
    }

    public  void setDestination(Destination destination){
        this.destination = destination;
    }

    public void enviarMensagem(final String to,
                               final String subject,
                               final String content) {

        MessageCreator messageCreator = new MessageCreator() {

            public Message createMessage(Session session) throws JMSException {
                MapMessage message = session.createMapMessage();
                message.setString("to", to);
                message.setString("subject", subject);
                message.setString("content", content);

                return message;
            }

        };

        System.out.println(jmsTemplate);
        jmsTemplate.send(destination, messageCreator);
    }



}
