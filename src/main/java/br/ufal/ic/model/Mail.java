package br.ufal.ic.model;

import java.io.Serializable;

/**
 * Created by rodrigo on 20/06/15.
 */
public class Mail implements Serializable {

    private String to;
    private String content;
    private String subject;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
