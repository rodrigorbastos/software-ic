package br.ufal.ic.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="material_reserve")
public class MaterialReserve implements Serializable{
	
	@Id
	@SequenceGenerator(name="material_reserve_id_seq", initialValue=1,
	allocationSize=1,sequenceName="material_reserve_id_seq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="material_reserve_id_seq")
	private int id;
	
	
	@ManyToOne
	@JoinColumn(name="client_id", referencedColumnName="id")
	private Client client;
	
	@OneToOne
	@JoinColumn(name="material_id", referencedColumnName="id")
	private Material material;
	
	@Column(name="description")
	private String description;
	
	@Column(name="start_date")
	private Timestamp startDate;
	
	@Column(name="end_date")
	private Timestamp endDate;
	
	public MaterialReserve(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	
	
	
	
	

}
