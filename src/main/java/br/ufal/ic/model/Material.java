package br.ufal.ic.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;


@Entity
@Table(name="material")
public class Material implements Serializable{
	
	@Id
	@SequenceGenerator(name="material_id_seq", initialValue=1, allocationSize=1, sequenceName="material_id_seq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="material_id_seq")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@ManyToOne
	@JoinColumn(name="material_type_id", referencedColumnName="id")
	private MaterialType materialType;
	
	@Column(name="code", unique=true)
	private String code;
	
	public Material(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MaterialType getMaterialType() {
		return materialType;
	}

	public void setMaterialType(MaterialType materialType) {
		this.materialType = materialType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	

}
