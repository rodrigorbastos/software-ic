package br.ufal.ic.model;


import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name="student")
public class Student implements Serializable{
	
	@Id
	@SequenceGenerator(name="student_id_seq",initialValue=1,
	allocationSize=1,sequenceName="student_id_seq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="student_id_seq")
	private int id;

	@OneToOne
	@JoinColumn(name="client_id", referencedColumnName="id")
	private Client client;

	@Column(name="matricula", unique=true)
	@NotNull(message = "Matricula Nulo")
	@NotEmpty(message = "Matrícula vazia")
	private String matricula;
	
	public Student(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}	

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", client=" + client + ", matricula="
				+ matricula + "]";
	}
	
	
}
