package br.ufal.ic.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="classroom")
public class Classroom implements Serializable{
	
	@Id
	@SequenceGenerator(name="classroom_id_seq", initialValue=1,
	allocationSize=1, sequenceName="classroom_id_seq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="classroom_id_seq")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="local")
	private String local;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}
	
	
	
	

	
	

}
