package br.ufal.ic.model;

import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="document")
public class Document implements Serializable{

	@Id
	@SequenceGenerator(name="document_id_seq",initialValue=1,
	allocationSize=1,sequenceName="document_id_seq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="document_id_seq")
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "client_id", referencedColumnName = "id")
	private Client client;

	@Transient
	private int client_id;

	@NotNull
	@NotEmpty(message = "Tipo de documento não pode ser vazio")
	@Column(name="doc_type")
	private String docType;

	@NotNull
	@NotEmpty(message = "Tipo de documento não pode ser vazio")
	@Column(name="file_name")
	private String fileName;
	
	@Column(name ="status")
	private String status;

	@NotEmpty(message = "Tipo de documento não pode ser vazio")
	@Size(max=140, message="Descrição precisa ter no máximo 140 caracteres")
	@Column(name="description")
	private String description;

	@NotNull
	@NotEmpty(message = "O link do documento não pode ser vazio")
	@Column(name="link")
	private String link;

	@Column(name="start_date")
	private Date startDate;
	
	@Column(name="end_date")
	private Date endDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdClient() {
		return client_id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	
	
	
}
