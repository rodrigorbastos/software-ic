package br.ufal.ic.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="material_type")
public class MaterialType implements Serializable{
	
	@Id
	@SequenceGenerator(name="material_type_id", initialValue=1, allocationSize=1, sequenceName="material_type_id")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="material_type_id")
	private int id;
	
	@Column(name="name", unique=true)
	private String name;
	
	public MaterialType(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "MaterialType [id=" + id + ", name=" + name + "]";
	}
	
	
	

}
