package br.ufal.ic.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="employees")
public class Employees implements Serializable {
	
	@Id
	@SequenceGenerator(name="employees_id_seq", initialValue=1,
	allocationSize=1, sequenceName="employees_id_seq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="employees_id_seq")
	private Integer id;
	
	@OneToOne
	@JoinColumn(name="client_id", referencedColumnName="id")
	private Client client;
	
	@Column(name="work_place")
	private String workPlace;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getWorkPlace() {
		return workPlace;
	}

	public void setWorkPlace(String workPlace) {
		this.workPlace = workPlace;
	}
	
	
	

}
