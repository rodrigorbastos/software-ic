package br.ufal.ic.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="classroom_reserve")
public class ClassroomReserve implements Serializable{
	
	@Id
	@SequenceGenerator(name="classroom_reserved_id_seq", initialValue=1,
	allocationSize=1, sequenceName="classroom_reserved_id_seq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="classroom_reserved_id_seq")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="client_id", referencedColumnName = "id")
	private Client client ;
	
	@ManyToOne
	@JoinColumn(name="classroom_id", referencedColumnName = "id")
	private Classroom classroom;
	
	@Column(name="start_date")
	private Date startDate;
	
	@Column(name="end_date")
	private Date endDate;
	
	
	@Column(name="observation")
	private String observation;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Client getClient() {
		return client;
	}


	public void setClient(Client client) {
		this.client = client;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getObservation() {
		return observation;
	}


	public void setObservation(String observation) {
		this.observation = observation;
	}


	public Classroom getClassroom() {
		return classroom;
	}


	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}
	
	
	
	
	
	
}
