package br.ufal.ic.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import br.ufal.ic.dao.ClassroomDAO;
import br.ufal.ic.model.Classroom;
import br.ufal.ic.model.Client;

public class ClassroomDAOImpl implements ClassroomDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Session getCurrentSession() {
        return this.sessionFactory.openSession();
    }

    public Classroom save(Classroom classroom) {
        Session session = getCurrentSession();
        Transaction tx = session.beginTransaction();
        session.save(classroom);
        tx.commit();
        session.close();
        return classroom;
    }

    public Classroom update(Classroom classroom) {
        Session session = getCurrentSession();
        Transaction tx = session.beginTransaction();
        session.update(classroom);
        tx.commit();
        session.close();
        return classroom;
    }

    public void delete(Classroom classroom) {
        Session session = getCurrentSession();
        Transaction tx = session.beginTransaction();
        session.delete(classroom);
        tx.commit();
        session.close();
    }

    public List<Classroom> findAll() {
    	Session session = getCurrentSession();
        List<Classroom> listClassroom = session.createCriteria(Classroom.class).list();
        session.close();
        return listClassroom;
    }

    public Classroom findById(int classroom_id) {
    	Session session = getCurrentSession();
        Criteria createCriteria = session.createCriteria(Classroom.class);

        createCriteria.add(Restrictions.eq("id", classroom_id));
    	Classroom c = null;
    	c = (Classroom) createCriteria.uniqueResult();
        session.close();
        return c;
    }

}
