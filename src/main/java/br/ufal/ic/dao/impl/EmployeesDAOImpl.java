package br.ufal.ic.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import br.ufal.ic.dao.EmployeesDAO;
import br.ufal.ic.model.Document;
import br.ufal.ic.model.Employees;
import br.ufal.ic.model.Teacher;

public class EmployeesDAOImpl implements EmployeesDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getCurrentSession() {
		return this.sessionFactory.openSession();
	}

	public Employees save(Employees employees) {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(employees);
		tx.commit();
		session.close();
		return employees;
		
	}

	public Employees update(Employees employees) {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.update(employees);
		tx.commit();
		session.close();
		return employees;
		
	}

	public void delete(Employees employees) {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.delete(employees);
		tx.commit();
		session.close();
		
	}

	public List<Employees> findAll() {
		Session session = getCurrentSession();
		List<Employees> listEmployees = session.createCriteria(Employees.class).list();
		session.close();
		return listEmployees;
	}

	public Employees findById(int employees_id) {
		Session session = getCurrentSession();
		Criteria createCriteria = session.createCriteria(
				Employees.class);

		createCriteria.add(Restrictions.eq("id", employees_id));
		Employees e = null;
		e = (Employees) createCriteria.uniqueResult();
		session.close();
		return e;
	}

}
