package br.ufal.ic.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import br.ufal.ic.dao.StudentDAO;
import br.ufal.ic.model.Student;

public class StudentDAOImpl implements StudentDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getCurrentSession() {
		return this.sessionFactory.openSession();
	}

	public Student save(Student student) throws Exception {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(student);
		tx.commit();
		session.close();
		return student;
	}

	public Student update(Student student) throws Exception {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.update(student);
		tx.commit();
		session.close();
		return student;
	}

	public void delete(Student student) throws Exception {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.delete(student);
		tx.commit();
		session.close();
	}

	public List<Student> findAll() {
		Session session = getCurrentSession();
		List<Student> listStudent =  session.createCriteria(Student.class).list();
		session.close();
		return listStudent;
	}

	public Student findById(int id) throws Exception {
		Session session = getCurrentSession();
		Criteria createCriteria = session.createCriteria(Student.class);
		
		createCriteria.add(Restrictions.eq("id", id));
		Student s = (Student) createCriteria.uniqueResult();
		session.close();
		
		return s; 
	}
}
