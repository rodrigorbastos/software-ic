package br.ufal.ic.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import br.ufal.ic.dao.MaterialTypeDAO;
import br.ufal.ic.model.MaterialType;

public class MaterialTypeDAOImpl implements MaterialTypeDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getCurrentSession() {
		return this.sessionFactory.openSession();
	}
        
	public MaterialType save(MaterialType materialType) throws Exception {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(materialType);
		tx.commit();
		session.close();

		return materialType;
	}

	public MaterialType update(MaterialType materialType) throws Exception {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.update(materialType);
		tx.commit();
		session.close();

		return materialType;
	}

	public MaterialType delete(MaterialType materialType) throws Exception {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.delete(materialType);
		tx.commit();
		session.close();
		return materialType;

	}

	public List<MaterialType> findAll() {
		Session session = getCurrentSession();
		List<MaterialType> listMT = session.createCriteria(MaterialType.class).list();
		session.close();
		return listMT;
	}

	public MaterialType findById(int id) throws Exception {
		Session session = getCurrentSession();
		Criteria createCriteria = session.createCriteria(
				MaterialType.class);

		createCriteria.add(Restrictions.eq("id", id));
		MaterialType mt = (MaterialType) createCriteria.uniqueResult();
		session.close();
		return mt;
	}

}
