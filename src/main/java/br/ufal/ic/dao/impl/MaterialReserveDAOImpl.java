package br.ufal.ic.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import br.ufal.ic.dao.MaterialReserveDAO;
import br.ufal.ic.model.ClassroomReserve;
import br.ufal.ic.model.Client;
import br.ufal.ic.model.MaterialReserve;

public class MaterialReserveDAOImpl implements MaterialReserveDAO {

	SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {

		return this.sessionFactory.openSession();
	}

	public MaterialReserve save(MaterialReserve materialReserve)
			throws Exception {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(materialReserve);
		tx.commit();
		session.close();

		return materialReserve;
	}

	public MaterialReserve update(MaterialReserve materialReserve)
			throws Exception {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.update(materialReserve);
		tx.commit();
		session.close();

		return materialReserve;
	}

	public MaterialReserve delete(MaterialReserve materialReserve)
			throws Exception {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.delete(materialReserve);
		tx.commit();
		session.close();

		return materialReserve;
	}

	public List<MaterialReserve> findAll() {
		Session session = getCurrentSession();
		List<MaterialReserve> listMR = session.createCriteria(MaterialReserve.class).list();
		session.close();
		return listMR;
	}

	public MaterialReserve findById(int id) throws Exception {
		Session session = getCurrentSession();
		Criteria createCriteria = session.createCriteria(
				MaterialReserve.class);
		MaterialReserve mr = null;
		
		createCriteria.add(Restrictions.eq("id", id));
		mr = (MaterialReserve) createCriteria.uniqueResult();
		session.close();
		return mr;
	}

	public MaterialReserve findByIdClient(Client client) {
		Session session = getCurrentSession();
		Criteria createCriteria = session.createCriteria(
				MaterialReserve.class);

		createCriteria.add(Restrictions.eq("client", client));
		MaterialReserve mr = null;
		mr = (MaterialReserve) createCriteria.uniqueResult();
		session.close();
		return mr;

		
	}

}
