package br.ufal.ic.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import br.ufal.ic.dao.TeacherDAO;
import br.ufal.ic.model.Document;
import br.ufal.ic.model.Student;
import br.ufal.ic.model.Teacher;

public class TeacherDAOImpl implements TeacherDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getCurrentSession() {
		return this.sessionFactory.openSession();
	}

	public Teacher save(Teacher teacher) throws Exception{
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(teacher);
		tx.commit();
		session.close();
		return teacher;
		
	}

	public Teacher update(Teacher teacher) throws Exception{
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.update(teacher);
		tx.commit();
		session.close();
		return teacher;
		
	}

	public void delete(Teacher teacher) throws Exception{
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.delete(teacher);
		tx.commit();
		session.close();
		
	}

	public List<Teacher> findAll() {
		Session session = getCurrentSession();
		List<Teacher> listTeacher = session.createCriteria(Teacher.class).list();
		session.close();
		return listTeacher;
	}

	public Teacher findById(int teacher_id) throws Exception {
		Session session = getCurrentSession();
		Criteria createCriteria = session.createCriteria(
				Teacher.class);

		createCriteria.add(Restrictions.eq("id", teacher_id));
		Teacher t =  (Teacher) createCriteria.uniqueResult();
		session.close();
		return t;
	}

}
