package br.ufal.ic.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import br.ufal.ic.dao.DocumentDAO;
import br.ufal.ic.model.Client;
import br.ufal.ic.model.Document;

public class DocumentDAOImpl implements DocumentDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getCurrentSession() {
		return this.sessionFactory.openSession();
	}

	public Document save(Document document) {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(document);
		tx.commit();
		session.close();
                
                return document;
	}

	public Document update(Document document) {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.update(document);
		tx.commit();
		session.close();
                
                return document;
	}

	public void delete(Document document) {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.delete(document);
		tx.commit();
		session.close();
	}

	public List<Document> findAll() {
		Session session = getCurrentSession();
		List<Document> listDoc = session.createCriteria(Document.class).list();
		session.close();
		return listDoc;
	}

	public Document findById(int document_id) {
		Session session = getCurrentSession();
		Criteria createCriteria = session.createCriteria(
				Document.class);

		createCriteria.add(Restrictions.eq("id", document_id));
		Document doc = null;
		doc = (Document) createCriteria.uniqueResult();
		session.close();
		return doc;
	}

	public Document findByClient(Client client) {
		Session session = getCurrentSession();
		Criteria createCriteria = session.createCriteria(
				Document.class);

		createCriteria.add(Restrictions.eq("client", client));
		Document doc = null;
		doc = (Document) createCriteria.uniqueResult();
		session.close();
		return doc;
	}

}
