package br.ufal.ic.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import br.ufal.ic.dao.ClassroomReserveDAO;
import br.ufal.ic.model.ClassroomReserve;
import br.ufal.ic.model.Client;

public class ClassroomReserveDAOImpl implements ClassroomReserveDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getCurrentSession() {
		return this.sessionFactory.openSession();
	}

	public ClassroomReserve save(ClassroomReserve classroomReserve) {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(classroomReserve);
		tx.commit();
		session.close();

		return classroomReserve;
	}

	public ClassroomReserve update(ClassroomReserve classroomReserve) {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.update(classroomReserve);
		tx.commit();
		session.close();

		return classroomReserve;
	}

	public void delete(ClassroomReserve classroomReserve) {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.delete(classroomReserve);
		tx.commit();
		session.close();
	}

	public List<ClassroomReserve> findAll() {
		Session session = getCurrentSession();
		List<ClassroomReserve> listCR = session.createCriteria(
				ClassroomReserve.class).list();
		session.close();
		return listCR;
	}

	public ClassroomReserve findById(int classroom_reserve_id) {

		Session session = getCurrentSession();
		Criteria createCriteria = session
				.createCriteria(ClassroomReserve.class);

		createCriteria.add(Restrictions.eq("id", classroom_reserve_id));
		ClassroomReserve cr = null;
		cr = (ClassroomReserve) createCriteria.uniqueResult();
		session.close();
		return cr;
	}

	public ClassroomReserve findByIdClient(Client client) {
		Session session = getCurrentSession();
		Criteria createCriteria = session.createCriteria(
				ClassroomReserve.class);

		createCriteria.add(Restrictions.eq("client", client));
		ClassroomReserve classroomReserve = null;
		classroomReserve = (ClassroomReserve) createCriteria.uniqueResult();
		session.close();
		return classroomReserve;
	}

}
