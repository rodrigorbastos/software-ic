package br.ufal.ic.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;

import br.ufal.ic.dao.MaterialDAO;
import br.ufal.ic.model.Material;

public class MaterialDAOImpl implements MaterialDAO {

	@Autowired
	private SessionFactory sessionFactory;

	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public Session getCurrentSession() {
		return sessionFactory.openSession();
	}

	public Material save(Material material) throws Exception {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(material);
		tx.commit();
		session.close();
		return material;

	}
	
	public Material update(Material material) throws Exception  {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.update(material);
		tx.commit();
		session.close();
		return material;

	}
	
	public void delete(Material material) throws Exception{
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.delete(material);
		tx.commit();
		session.close();

	}

	public List<Material> findAll() {
		Session session = getCurrentSession();
		List<Material> listMaterial = session.createCriteria(Material.class).list();
		session.close();
		return listMaterial;
	}

	public Material findById(int id)  throws Exception{
		Session session = getCurrentSession();
		Criteria createCriteria = session.createCriteria(Material.class);
		
		createCriteria.add(Restrictions.eq("id", id));
		Material m = null;
		m = (Material) createCriteria.uniqueResult();
		session.close();
		return m;
	}

}
