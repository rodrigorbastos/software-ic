package br.ufal.ic.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.ufal.ic.dao.ClientDAO;
import br.ufal.ic.model.Client;

@Repository
public class ClientDAOImpl implements ClientDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getCurrentSession() {
		return sessionFactory.openSession();
	}

	public Client save(Client client) {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(client);
		tx.commit();
		return client;
	}

	public Client update(Client client) {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.update(client);
		tx.commit();
		return client;
	}

	public void delete(Client client) {
		Session session = getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.delete(client);
		tx.commit();
		
	}

	public List<Client> findAll() {
		
		Session session = getCurrentSession();
		List<Client> listClient = session.createCriteria(Client.class).list();
		session.close();

		return listClient;
	}

	public Client findById(int client_id) {
		Session session = getCurrentSession();
		Criteria createCriteria = session.createCriteria(Client.class);
		
		createCriteria.add(Restrictions.eq("id", client_id));
		Client c = (Client) createCriteria.uniqueResult();
		
		session.close();
		return c;
	}
	
}
