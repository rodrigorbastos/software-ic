package br.ufal.ic.dao;

import java.util.List;

import br.ufal.ic.model.Client;

public interface ClientDAO {

	/*
	 * CRUD CLIENT
	 */

	public Client save(Client client);

	public Client update(Client client);

	public void delete(Client client);

	/*
	 * AUXILIARES
	 */

	public Client findById(int client_id);

	public List<Client> findAll();

}
