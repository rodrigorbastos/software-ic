package br.ufal.ic.dao;

import java.util.List;

import br.ufal.ic.model.Student;




public interface StudentDAO {

	/*
	 *CRUD STUDENT
	 */
	public Student save(Student student) throws Exception;
	
	public Student update(Student student) throws Exception;
	
	public void delete(Student student) throws Exception;
	
	/*
	 * AUXILIARES
	 */
    
    public List<Student> findAll();
    
    public Student findById(int id) throws Exception;
}
