package br.ufal.ic.dao;

import java.util.List;

import br.ufal.ic.model.Employees;

public interface EmployeesDAO {

	/*
	 *CRUD STUDENT
	 */
	public Employees save(Employees employees)throws Exception;
	
	public Employees update(Employees employees)throws Exception;
	
	public void delete(Employees employees)throws Exception;
	
	/*
	 * AUXILIARES
	 */
    
    public List<Employees> findAll();
    
    public Employees findById(int employees_id)throws Exception;
}
