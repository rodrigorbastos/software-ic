package br.ufal.ic.dao;

import java.util.List;

import br.ufal.ic.model.ClassroomReserve;
import br.ufal.ic.model.Client;

public interface ClassroomReserveDAO {

	
	/*
	 * CRUD CLASSROOM Reserve
	 */
	public ClassroomReserve save(ClassroomReserve classroomReserve) throws Exception;
	
	public ClassroomReserve update(ClassroomReserve classroomReserve) throws Exception;
	
	public void delete(ClassroomReserve classroomReserve) throws Exception;
    
	/*
	 * AUXILIARES
	 */
    public List<ClassroomReserve> findAll();
    
    public ClassroomReserve findById(int classroom_reserve_id) throws Exception;

    public ClassroomReserve findByIdClient(Client client) throws Exception;
}
