package br.ufal.ic.dao;

import java.util.List;

import org.postgresql.util.PSQLException;

import br.ufal.ic.model.Material;

public interface MaterialDAO{
	/*
	 * CRUD MATERIAL
	 */
	public Material save(Material material) throws Exception;
	
	public Material update(Material material) throws Exception;
	
	public void delete(Material material) throws Exception;
    
	/*
	 * AUXILIARES
	 */
    public List<Material> findAll();
    
    public Material findById(int id) throws Exception;
	

}
