package br.ufal.ic.dao;

import java.util.List;

import br.ufal.ic.model.Student;
import br.ufal.ic.model.Teacher;

public interface TeacherDAO {
	
	/*
	 *CRUD STUDENT
	 */
	public Teacher save(Teacher teacher) throws Exception;
	
	public Teacher update(Teacher teacher) throws Exception;
	
	public void delete(Teacher teacher) throws Exception;
	
	/*
	 * AUXILIARES
	 */
    
    public List<Teacher> findAll();
    
    public Teacher findById(int teacher_id) throws Exception;

}
