package br.ufal.ic.dao;

import java.util.List;

import br.ufal.ic.model.Client;
import br.ufal.ic.model.Document;

public interface DocumentDAO {
	
	
	/*
	 *CRUD DOCUMENT
	 */
	public Document save(Document document) throws Exception;
	
	public Document update(Document document)throws Exception;
	
	public void delete(Document document)throws Exception;
	
	/*
	 * AUXILIARES
	 */
    
    public List<Document> findAll();
    
    public Document findById(int document_id)throws Exception;

    public Document findByClient(Client client)throws Exception;

}
