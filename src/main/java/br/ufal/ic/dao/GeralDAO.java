package br.ufal.ic.dao;

import java.util.List;

import br.ufal.ic.model.Client;

public interface GeralDAO {
	
	public void save(Object object);
    
    public List<Object> list();
    
    public  Object getEntity(int id);


}
