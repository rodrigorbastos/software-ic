package br.ufal.ic.dao;

import java.util.List;

import br.ufal.ic.model.Client;
import br.ufal.ic.model.MaterialReserve;

public interface MaterialReserveDAO {
	
	/*
	 * CRUD MATERIAL RESERVE
	 */
	
	public MaterialReserve save(MaterialReserve materialReserve) throws Exception;
	
	public MaterialReserve update(MaterialReserve materialReserve) throws Exception;
	
	public MaterialReserve delete(MaterialReserve materialReserve) throws Exception;
	
	
	/*
	 * QUERY
	 */
	
	public List<MaterialReserve> findAll();
	
	public MaterialReserve findById(int id) throws Exception;

	public MaterialReserve findByIdClient(Client client)throws Exception;

}
