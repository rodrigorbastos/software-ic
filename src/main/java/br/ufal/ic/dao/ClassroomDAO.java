package br.ufal.ic.dao;

import java.util.List;

import br.ufal.ic.model.Classroom;

public interface ClassroomDAO {

	/*
	 * CRUD CLASSROOM 
	 */
	public Classroom save(Classroom classroom)throws Exception;

	public Classroom update(Classroom classroom)throws Exception;

	public void delete(Classroom classroom)throws Exception;

	/*
	 * AUXILIARES
	 */
	public List<Classroom> findAll();

	public Classroom findById(int classroom_id)throws Exception;

}
