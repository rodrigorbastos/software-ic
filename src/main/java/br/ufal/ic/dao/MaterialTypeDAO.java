package br.ufal.ic.dao;

import java.util.List;

import br.ufal.ic.model.MaterialType;

public interface MaterialTypeDAO {
	
	/*
	 * CRUD MATERIAL TYPE
	 */
	public MaterialType save(MaterialType materialType) throws Exception;
	
	public MaterialType update(MaterialType materialType) throws Exception;

	public MaterialType delete(MaterialType materialType) throws Exception;

	
	
	
    /*
     * AUXILIARES
     */
    public List<MaterialType> findAll();
    
    public  MaterialType findById(int id) throws Exception;

}
